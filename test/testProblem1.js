
const path = require('path');
const pathProblem1 = path.join(__dirname,"../problem1.js");


const problem1 = require(pathProblem1   );

let dummyData=[
        {
            batsman: 'srk',
            bowler: 'Arbaz',
            player_dismissed: 'srk',
            dismissal_kind: 'caught'
        },
        {
            batsman: 'Sohel',
            bowler: 'Arbaz',
            player_dismissed: 'Sohel',
            dismissal_kind: 'caught'
        },
        {
            batsman: 'Sohel',
            bowler: 'Arbaz',
            player_dismissed: 'Sohel',
            dismissal_kind: 'caught'
        },
        {
            batsman: 'srk',
            bowler: 'Arbaz',
            player_dismissed: 'srk',
            dismissal_kind: 'caught'
        },
        {
            batsman: 'srk',
            bowler: 'Arbaz',
            player_dismissed: 'srk',
            dismissal_kind: 'run out'
        },
        {
            batsman: 'Sohel',
            bowler: 'Arbaz',
            player_dismissed: 'Sohel',
            dismissal_kind: 'caught'
        },
        {
            batsman: 'Sohel',
            bowler: 'Arbaz',
            player_dismissed: 'Sohel',
            dismissal_kind: 'run out'
        },
        {
            batsman: 'srk',
            bowler: 'Arbaz',
            player_dismissed: 'srk',
            dismissal_kind: 'caught'
        }
    ]
 
problem1(dummyData);