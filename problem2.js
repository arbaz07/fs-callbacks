/*
Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/
const path = require("path");
const readFile = path.join(__dirname,"./lipsum.txt");
const pathFile1 = path.join(__dirname,"./newFile1.txt");
const pathFile2 = path.join(__dirname,"./newFile2.txt");
const pathFile3 = path.join(__dirname,"./newFile3.txt");
const fileNames = path.join(__dirname,"./Filenames.txt");
const regex = ". \n";
const fs=require('fs');
const fsPromise = fs.promises;

function problem2(){
    fsPromise.readFile(readFile,"utf-8")
    .then((dataFromLipsum)=>{
        console.log("Read operation on lipsum.txt is completed");
        let upperCaseContent = dataFromLipsum.toUpperCase();
        console.log("Converted into upperCase");
        const promise1 = fsPromise.writeFile(pathFile1,upperCaseContent);
        const promise2 = fsPromise.writeFile(fileNames,pathFile1+regex);
        return Promise.all([promise1,promise2])
    })
    .then(()=>{
        console.log("Uppercase Data Writting on a new file "+pathFile1+" completed.");
        console.log("Writting new file name on to a file "+fileNames);
        return fsPromise.readFile(pathFile1,"utf-8")
    })
    .then((data)=>{
        console.log("Completed Reading "+ pathFile1 +" reading completed")
        let lowerCaseFileContent = data.toLowerCase();
        let sentences = lowerCaseFileContent.split(". ");

        const promise1 = fsPromise.writeFile(pathFile2,sentences);
        const promise2 = fsPromise.appendFile(fileNames,pathFile2+regex);
        return Promise.all([promise1,promise2])
    })
    .then(()=>{
        console.log("Writting sentences to a new file "+pathFile2);
        console.log("appending new file name to a file "+fileNames);
        return fsPromise.readFile(pathFile2,"utf-8")
    })
    .then((dataFromNewFile2)=>{
        let arrayOfWord = dataFromNewFile2.split(" ").sort();
        console.log("Sorting file content "+pathFile2+" Done.");
        const promise3 = fsPromise.writeFile(pathFile3,arrayOfWord)
        const promise4 = fsPromise.appendFile(fileNames,pathFile3+regex);
        return Promise.all([promise3,promise4])
    })
    .then(()=>{
        console.log("Writting sorted data to a new file "+pathFile3);
        console.log("appending new file name to a file "+fileNames);
        return fsPromise.readFile(fileNames,"utf-8")
    })
    .then((fileNames)=>{
        let arrayOfFileName = fileNames.split(regex);
        // console.log(arrayOfFileName);
        let allFileDeleteStatus=[];
        // arrayOfFileName.forEach((fileName)=>{
        for(let index=0; index<arrayOfFileName.length-1; index++){
            console.log("delet request is made for "+arrayOfFileName[index]);
            allFileDeleteStatus.push(fsPromise.unlink(arrayOfFileName[index]));
        }
        return Promise.all(allFileDeleteStatus);
    })
    .then(()=>{
        console.log("Process Completed");
    })
    .catch(errorFunction);
}
function errorFunction(err){
    console.error(err);
}

problem2();

module.exports = problem2;
/*

fs.readFile("./lipsum.txt","utf-8",(err, contentOfLipsum)=>{
    if(err){
        console.error(err);
    }else{

        console.log("Read operation on lipsum.txt is completed");

        let upperCaseContent = contentOfLipsum.toUpperCase();
        console.log("Converted into upperCase");

        let newFileName = "newFile1.txt";
        fs.writeFile("./newFile1.txt",upperCaseContent,(err)=>{
            if(err){
                console.error(err);
            }else{

                console.log("Uppercase Data Writting on a new file newFile1.txt completed");

                fs.readFile("./filenames.txt","utf-8",(err,fileNames)=>{
                    if(err){
                        console.error(err);
                    }else{
                        console.log("Reading filenames.txt to get the name of newest file which is newFile1.txt  ")
                        let arr = fileNames.split(". ");
                        let fileName = arr[arr.length-2];

                        fs.readFile(fileName,"utf-8",(err, fileContent)=>{
                            if(err){
                                console.error(err);
                            }else{
                           --   console.log("Completed Reading newFile1.txt reading completed")

                                let lowerCaseFileContent = fileContent.toLowerCase();
                                let sentences = lowerCaseFileContent.split(". ")
                                // console.log(sentences);

                                let newFileName = "newFile2.txt";

                                fs.writeFile(`./${newFileName}`,sentences.toString(),"utf-8",(err)=>{
                                    if(err){
                                        console.error(err);
                                    }else{
                                        console.log("Writting sentences to a new file nawFile2.txt");

                                        fs.readFile("./filenames.txt","utf-8",(err, fileContent)=>{
                                            if(err){
                                                console.error(err);
                                            }else{
                                                console.log("Reading filenames.txt to get the name of newest file which is newFile2.txt  ")
                                                let arr = fileContent.split(". ");

                                                //last element is empty string regex ". "=> added in file name
                                                let filename = arr[arr.length-2];

                                                fs.readFile(`./${filename}`,"utf-8",(err,fileContent)=>{
                                                    if(err){
                                                        console.error(err);
                                                    }else{
                                                        let newFileName = "newFile3.txt";
                                                        let arrayOfWord = fileContent.split(" ").sort();
                                                        console.log("Read newFile2.txt sort its conetent")

                                                        fs.writeFile(`./${newFileName}`,arrayOfWord.toString(),"utf-8",(err)=>{
                                                            if(err){
                                                                console.error(err);
                                                            }else{
                                                                console.log("Writting sorted text to a new file nawFile3.txt");

                                                                fs.readFile("./filenames.txt","utf-8",(err,fileContent)=>{
                                                                    if(err){
                                                                        console.error(err);
                                                                    }else{
                                                                        console.log("Reading filenames.txt to get the name of newest file which is newFile3.txt  ")
                                                                        let listOfFileNames = fileContent.split(". ");

                                                                        listOfFileNames.forEach(function(listItem, index){
                                                                            fs.unlink(listItem,()=>{

                                                                            });    

                                                                        });
                                                                    }
                                                                    
                                                                });
                                                            }
                                                    

                                                        });

                                                        fs.appendFile("filenames.txt",newFileName+". " , (err) => {
                                                            console.log("appended new file name newFile3.txt to filename.txt")
                                                        });
                                                    }

                                                });
                                            }

                                        });
                                    }

                                });

                                fs.appendFile("filenames.txt", newFileName+". ", (err) => {
                                    console.log("appended new file name newFile2.txt to filename.txt")
                                });
                            }
                        
                        });

                    }

                });

            }

        });

        fs.appendFile("filenames.txt", newFileName+". ", (err) => {
            console.log("appended new file name newFile1.txt to filename.txt")
        });
    }

});
*/



// module.exports = function problem2(){
//     fs.readFile("./lipsum.txt","utf-8",cb_methode1);
// }



// function cb_methode1(err, contentOfLipsum){
    
//     if(err){
//         console.error(err);
//     }else{

//         console.log("Read operation on lipsum.txt is completed");

//         let upperCaseContent = contentOfLipsum.toUpperCase();
//         console.log("Converted into upperCase");

//         let newFileName = "newFile1.txt";
//         fs.writeFile("./newFile1.txt",upperCaseContent,cb_methode2);

//         fs.appendFile("filenames.txt", newFileName+". ", (err) => {
//             if(err){
//                 console.error(err);
//             }else{
//                 console.log("appended new file name newFile1.txt to filename.txt")
//             }
//         });
//     }
// }

// function cb_methode2(err){

//     if(err){
//         console.error(err);
//     }else{

//         console.log("Uppercase Data Writting on a new file newFile1.txt completed");

//         fs.readFile("./filenames.txt","utf-8",cb_methode3);
//     }
// }


// function cb_methode3(err,fileNames){
//     if(err){
//         console.error(err);
//     }else{
//         console.log("Reading filenames.txt to get the name of newest file which is newFile1.txt  ")
//         let arr = fileNames.split(". ");
//         let fileName = arr[arr.length-2];

//         fs.readFile(fileName,"utf-8",cb_methode4);
//     }
// }



// function cb_methode4(err, fileContent){
//     if(err){
//         console.error(err);
//     }else{
//         console.log("Completed Reading newFile1.txt reading completed")

//         let lowerCaseFileContent = fileContent.toLowerCase();
//         let sentences = lowerCaseFileContent.split(". ")
//         // console.log(sentences);

//         let newFileName = "newFile2.txt";

//         fs.writeFile(`./${newFileName}`,sentences.toString(),"utf-8",cb_methode5);

//         fs.appendFile("filenames.txt", newFileName+". ", (err) => {
//             if(err){
//                 console.error(err);
//             }else{
//                 console.log("appended new file name newFile2.txt to filename.txt")
//             }
//         });
//     }
// }



// function cb_methode5(err){
//     if(err){
//         console.error(err);
//     }else{
//         console.log("Writting sentences to a new file nawFile2.txt");

//         fs.readFile("./filenames.txt","utf-8",cb_methode6);
//     }
// }



// function cb_methode6(err, fileContent){
//     if(err){
//         console.error(err);
//     }else{
//         console.log("Reading filenames.txt to get the name of newest file which is newFile2.txt  ")
//         let arr = fileContent.split(". ");

//         //last element is empty string regex ". "=> added in file name
//         let filename = arr[arr.length-2];

//         fs.readFile(`./${filename}`,"utf-8",cb_methode7);
//     }
// }



// function cb_methode7(err,fileContent){
//     if(err){
//         console.error(err);
//     }else{
//         let newFileName = "newFile3.txt";
//         let arrayOfWord = fileContent.split(" ").sort();
//         console.log("Read newFile2.txt sort its conetent")

//         fs.writeFile(`./${newFileName}`,arrayOfWord.toString(),"utf-8",cb_methode8);

//         fs.appendFile("filenames.txt",newFileName+". " , (err) => {
//             if(err){
//                 console.error(err);
//             }else{
//                 console.log("appended new file name newFile3.txt to filename.txt")
//             }
//         });
//     }
// }




// function cb_methode8(err){
//     if(err){
//         console.error(err);
//     }else{
//         console.log("Writting sorted text to a new file nawFile3.txt");

//         fs.readFile("./filenames.txt","utf-8",cb_methode9);
//     }
// }



// function cb_methode9(err,fileContent){
//     if(err){
//         console.error(err);
//     }else{
//         console.log("Reading filenames.txt to get the name of newest file which is newFile3.txt  ")
//         let listOfFileNames = fileContent.split(". ");

//         listOfFileNames.forEach(function(listItem){
//             if(listItem!==''){
//                 fs.unlink(listItem,(err)=>{
//                     if(err){
//                         console.error(err);
//                     }else{
//                         console.log("One file got deleted :- "+listItem);
//                     }
//                 });    
//             }
//         });
//     }   
// }