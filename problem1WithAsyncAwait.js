const path = require("path");
const fs = require("fs");
const fsPromise = fs.promises;
const dummyData = require("./data"); 



const folderPath = path.join(__dirname,"./CreateNewFolder");
const randomNum = Math.floor(Math.random() * 100) + 1;


async function fileHandler(pathForCreatingFolder,data){

    try{
        await fsPromise.mkdir(pathForCreatingFolder,true)
        
        console.log("Dir created");
        let fileWritePromises = [];
        for (let fileCount = 1; fileCount <=randomNum ; fileCount++) {
            let writePromise = fsPromise.writeFile(`${pathForCreatingFolder}/newFile_${fileCount}.json`, JSON.stringify(data)) 
            console.log("file write call for "+ `${pathForCreatingFolder}/newFile_${fileCount}.json`)  ;  
            fileWritePromises.push(writePromise);
        }

        await Promise.all(fileWritePromises);
        
        let fileDeletePromises = [];
        console.log("Starting delete operation");
        for (let fileCount = 1;fileCount <= randomNum;fileCount++) {
            let deletePromise = fsPromise.unlink(`${pathForCreatingFolder}/newFile_${fileCount}.json`);
            console.log("file delet call for "+ `${pathForCreatingFolder}/newFile_${fileCount}.json`)  ;  
            fileDeletePromises.push(deletePromise); 
        }

        await Promise.all(fileDeletePromises);

        await fsPromise.rmdir(pathForCreatingFolder);
        
        console.log("Process completed");

    }catch(err){
        console.error(err);
    }
}



fileHandler(folderPath,dummyData);

module.exports = fileHandler;
