/*
Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/
const path = require("path");
const readFile = path.join(__dirname,"./lipsum.txt");
const pathFile1 = path.join(__dirname,"./newFile1.txt");
const pathFile2 = path.join(__dirname,"./newFile2.txt");
const pathFile3 = path.join(__dirname,"./newFile3.txt");
const fileNames = path.join(__dirname,"./Filenames.txt");
const regex = ". \n";
const fs=require('fs');
const fsPromise = fs.promises;

async function problem2(){
    try{
        let dataFromLipsum = await fsPromise.readFile(readFile,"utf-8")

        console.log("Read operation on lipsum.txt is completed");
        let upperCaseContent = dataFromLipsum.toUpperCase();
        console.log("Converted into upperCase");
        const promise1 = fsPromise.writeFile(pathFile1,upperCaseContent);
        const promise2 = fsPromise.writeFile(fileNames,pathFile1+regex);
        
        await Promise.all([promise1,promise2])
    
        console.log("Uppercase Data Writting on a new file "+pathFile1+" completed.");
        console.log("Writting new file name on to a file "+fileNames);

        let data = await fsPromise.readFile(pathFile1,"utf-8")
  
        console.log("Completed Reading "+ pathFile1 +" reading completed")
        let lowerCaseFileContent = data.toLowerCase();
        let sentences = lowerCaseFileContent.split(". ");

        const promise3 = fsPromise.writeFile(pathFile2,sentences);
        const promise4 = fsPromise.appendFile(fileNames,pathFile2+regex);

        await Promise.all([promise3,promise4])

        console.log("Writting sentences to a new file "+pathFile2);
        console.log("appending new file name to a file "+fileNames);

        let dataFromNewFile2 =  await fsPromise.readFile(pathFile2,"utf-8")
 
        let arrayOfWord = dataFromNewFile2.split(" ").sort();
        console.log("Sorting file content "+pathFile2+" Done.");
        const promise5 = fsPromise.writeFile(pathFile3,arrayOfWord)
        const promise6 = fsPromise.appendFile(fileNames,pathFile3+regex);

        await Promise.all([promise5,promise6])
 
        console.log("Writting sorted data to a new file "+pathFile3);
        console.log("appending new file name to a file "+fileNames);

        let nameOfFile = await fsPromise.readFile(fileNames,"utf-8")

        let arrayOfFileName = nameOfFile.split(regex);
        let allFileDeleteStatus=[];
        for(let index=0; index<arrayOfFileName.length-1; index++){
            let arr = arrayOfFileName[index].split("/");
            console.log("delet request is made for "+arr[arr.length-1]);
            allFileDeleteStatus.push(fsPromise.unlink(arrayOfFileName[index]));
        }

        await Promise.all(allFileDeleteStatus);
   
        console.log("Process Completed");

    }catch(err){
        console.error(err);
    }
}
problem2();

module.exports = problem2;