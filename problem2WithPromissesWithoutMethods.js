const path = require("path");
const readFile = path.join(__dirname, "./lipsum.txt");
const pathFile1 = path.join(__dirname, "./newFile1.txt");
const pathFile2 = path.join(__dirname, "./newFile2.txt");
const pathFile3 = path.join(__dirname, "./newFile3.txt");
const fileNames = path.join(__dirname, "./Filenames.txt");
const regex = ". \n";
const fs = require("fs");

function problem2() {
    new Promise((resolve, reject) => {
        return readFromFile(resolve, reject, readFile);
    })
    .then((dataFromLipsum) => {
        let upperCaseContent = dataFromLipsum.toUpperCase();
        const promise1 = new Promise((resolve, reject) => {
            writeOnFile(resolve, reject, pathFile1, upperCaseContent);
        });

        const promise2 = new Promise((resolve, reject) => {
            writeOnFile(resolve, reject, fileNames, pathFile1 + regex);
        });
        return Promise.all([promise1, promise2]);
    })
    .then(() => {
        return new Promise((resolve, reject) => {
            return readFromFile(resolve, reject, pathFile1);
        });
    })
    .then((data) => {
        let lowerCaseFileContent = data.toLowerCase();
        let sentences = lowerCaseFileContent.split(". ");

        const promise3 = new Promise((resolve, reject) => {
            writeOnFile(resolve, reject, pathFile2, sentences);
        });
        const promise4 = new Promise((resolve, reject) => {
            appendOnFile(resolve, reject, fileNames, pathFile2 + regex);
        });
        return Promise.all([promise3, promise4]);
    })
    .then(() => {
        return new Promise((resolve, reject) => {
            return readFromFile(resolve, reject, pathFile2);
        });
    })
    .then((dataFromNewFile2) => {
        let arrayOfWord = dataFromNewFile2.split(" ").sort();
        const promise5 = new Promise((resolve, reject) => {
            appendOnFile(resolve, reject, fileNames, pathFile3 + regex);
        });
        const promise6 = new Promise((resolve, reject) => {
            appendOnFile(resolve, reject, pathFile3, arrayOfWord);
        });
        return Promise.all([promise5, promise6]);
    })
    .then(() => {
        return new Promise((resolve, reject) => {
            return readFromFile(resolve, reject, fileNames);
        });
    })
    .then((filenames) => {
        let arrayOfFileName = filenames.split(regex);
        let allFileDeleteStatus = [];
        for (let index = 0; index < arrayOfFileName.length - 1; index++) {
            let deletFilePromis = new Promise((resolve, reject) => {
                deleteFile(resolve, reject, arrayOfFileName[index]);
            });
            allFileDeleteStatus.push(deletFilePromis);
        }
        return Promise.all(allFileDeleteStatus);
    })
    .then(() => {
        console.log("Process Completed");
    })
    .catch(errorFunction);
}
function errorFunction(err) {
console.error(err);
}

problem2();

function writeOnFile(resolve, rejects, pathForCreatingFile, dataToWriteOnFile) {
fs.writeFile(
    pathForCreatingFile,
    dataToWriteOnFile.toString(),
    "utf-8",
    (err) => {
        if (err) {
            rejects("Error Occured While Writing on to a file : " + err);
        } else {
            let temp = pathForCreatingFile.split("/");
            let filename = temp[temp.length - 1];
            console.log("Writing on a file Done For : " + filename);
            resolve("OK");
        }
    });
}

function appendOnFile(resolve, rejects, path, data) {
    fs.appendFile(path, data.toString(), "utf-8", (err) => {
        if (err) {
            rejects("Error Occured While Writing on to a file : " + err);
        } else {
            let temp = path.split("/");
            let filename = temp[temp.length - 1];
            console.log("Writing on a file Done For : " + filename);
            resolve("OK");
        }
    });
}

function readFromFile(resolve, rejects, pathForReadingFile) {
    fs.readFile(pathForReadingFile, "utf-8", (err, data) => {
        if (err) {
            rejects("Error Occured While Reading the file : " + err);
        } else {
            let temp = pathForReadingFile.split("/");
            let filename = temp[temp.length - 1];
            console.log("File Readding Done For : " + filename);
            resolve(data);
        }
    });
}

function deleteFile(resolve, rejects, pathForDeletingFile) {
    fs.unlink(pathForDeletingFile, (err) => {
        if (err) {
            rejects("Error Occured While Deleting a file : " + err);
        } else {
            let temp = pathForDeletingFile.split("/");
            let filename = temp[temp.length - 1];
            console.log("File Deleted : " + filename);
            resolve("OK");
        }
    });
}
