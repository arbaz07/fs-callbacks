const path = require("path");
const fs = require("fs");
const dummyData = require("./data"); 






const folderPath = path.join(__dirname,"./CreateNewFolder");

const randomNum = Math.floor(Math.random() * 100) + 1;


function fileHandler(pathForCreatingFolder,data){

    new Promise((resolve,rejects)=>{
        createDIR(resolve,rejects,pathForCreatingFolder);
    })
    .then(()=>{
        let fileWritePromises = [];
        for (let fileCount = 1; fileCount <=randomNum ; fileCount++) {
            let currentFileName = `${pathForCreatingFolder}/newFile_${fileCount}.json`;
            let writePromise = new Promise((rosolve,rejects)=>{
                createFile(rosolve,rejects,currentFileName,JSON.stringify(data));
            });
            fileWritePromises.push(writePromise);
        }
        return Promise.all(fileWritePromises);
    })
    .then(()=>{
        let fileDeletePromises = [];
        console.log("Starting delete operation");
        for (let fileCount = 1;fileCount <= randomNum;fileCount++) {
            let filename = `${pathForCreatingFolder}/newFile_${fileCount}.json`;
            let deletePromise =new Promise((resolve,rejects)=>{
                deleteFile(resolve,rejects,filename);
            }) 
            fileDeletePromises.push(deletePromise); 
        }
        return Promise.all(fileDeletePromises);
    })
    .then(()=>{
        let deleteDirectoryPromise = new Promise((resolve,rejects)=>{
            deletDIR(resolve,rejects,pathForCreatingFolder)
        })
        return deleteDirectoryPromise;
    })
    .catch((err)=>{
        console.log("errro");
        console.error(err);

    })
   
}

function createDIR(resolve,rejects,pathForCreatingFolder){
    fs.mkdir(pathForCreatingFolder,true,(err)=>{
        if(err){
            rejects("Error Occured While Creating a Directory : "+err);
        }else{
            console.log("Dir created");
            resolve("OK");
        }
    });
}



function deletDIR(resolve,rejects,pathForDeletingFolder){
    fs.rmdir(pathForDeletingFolder,(err)=>{
        if(err){
            rejects("Error Occured While Deleting a Directory : "+err);
        }else{
            console.log("Dir Got Deleted");
            resolve("OK");
        }
    });
}



function createFile(resolve,rejects,pathForCreatingFile,dataToWriteOnFile){
    fs.writeFile(pathForCreatingFile,dataToWriteOnFile,"utf-8",(err)=>{
        if(err){
            rejects("Error Occured While Creating a file : "+err);
        }else{
            let temp = pathForCreatingFile.split("/");
            let filename = temp[temp.length-1];
            console.log("Write File Fone For : "+ filename)  ;  
            resolve("OK");
        }
    });
}


function deleteFile(resolve,rejects,pathForDeletingFile){
    fs.unlink(pathForDeletingFile,(err)=>{
        if(err){
            rejects("Error Occured While Deleting a file : "+err);
        }else{
            let temp = pathForDeletingFile.split("/");
            let filename = temp[temp.length-1];
            console.log("File Deleted : "+ filename)  ;  
            resolve("OK");
        }
    });
}
    

fileHandler(folderPath,dummyData);

module.exports = fileHandler;
